package Domen;

import java.util.HashMap;

public interface DepartamentRepository extends GenericRepository {
    HashMap<Long, Deportament> Create(Deportament dep);
    Deportament getEntity(long id);
    void Update(long id,Deportament dep);
}
